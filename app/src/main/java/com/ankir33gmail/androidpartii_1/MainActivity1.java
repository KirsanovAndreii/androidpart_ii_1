package com.ankir33gmail.androidpartii_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import static com.ankir33gmail.androidpartii_1.R.string.nullDec;

public class MainActivity1 extends AppCompatActivity {
    private EditText eT1;
    private EditText eT2;
    private EditText eT3;
    private TextView tV1;
    private TextView tV2;
    private TextView tV3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        eT1 = (EditText) findViewById(R.id.editText1);
        eT2 = (EditText) findViewById(R.id.editText2);
        eT3 = (EditText) findViewById(R.id.editText3);
        tV1 = (TextView) findViewById(R.id.textView1);
        tV2 = (TextView) findViewById(R.id.textView2);
        tV3 = (TextView) findViewById(R.id.textView3);
        eT1.addTextChangedListener(textWatcherP);
        eT2.addTextChangedListener(textWatcherP);
        eT3.addTextChangedListener(textWatcherP);
    }
   private TextWatcher textWatcherP = new TextWatcher() {
       @Override
       public void beforeTextChanged(CharSequence s, int start, int count, int after) {

       }

       @Override
       public void onTextChanged(CharSequence s, int start, int before, int count) {

       }

       @Override
       public void afterTextChanged(Editable s) {
           tV1.setText(translate(eT1.getText().toString(),2));
           tV2.setText(translate(eT2.getText().toString(),8));
           tV3.setText(translate(eT3.getText().toString(),16));

       }
   };
// вся математика тут
   public String translate (String n, int nSyst)
   {
       if (n.length() == 0) {return "нет данных";
       int numberWhole = 0;
       try {
            numberWhole = Integer.parseInt(n);   // если не число -> обнуление поля
       }
       catch (Exception e)
       {
           if (nSyst == 2) {eT1.setText(R.string.nullDec);
               eT1.setHint(R.string.noooNumber);}
           if (nSyst == 8) {eT2.setText(R.string.nullDec);
               eT2.setHint(R.string.noooNumber);}
           if (nSyst == 16) {eT3.setText(R.string.nullDec);
               eT3.setHint(R.string.noooNumber);}
           return "нет данных";
       }                                  // если число - проводим вычисления
       int remainder = 0;
       ArrayList <Integer> rezult = new ArrayList<>();
       if (numberWhole == 0) return "0";
       while (numberWhole != 0)
       {
          remainder =  numberWhole%nSyst;
          numberWhole = (int) numberWhole/nSyst;
          rezult.add(0, remainder);


       }

       return rezult.toString();
   }

}
